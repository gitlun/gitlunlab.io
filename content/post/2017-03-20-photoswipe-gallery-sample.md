---
title: Member Photos
subtitle: Making a Gallery
date: 2019-07-07
---

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/duan.jpg" caption="Founder & CIO">}}
  {{< figure thumb="-thumb" link="/img/fire.jpg" caption="Kindle" >}}
  {{< figure thumb="-thumb" link="/img/laser.jpg" caption="Shaprness" alt="This is a long comment about a triangle" >}}
{{< /gallery >}}
{{< load-photoswipe >}}
